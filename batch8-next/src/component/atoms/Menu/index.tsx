"use client";

import Link from "next/link";
import { usePathname } from "next/navigation";

const menuHeader = [
    { name: "Gallery", href: "/gallery" },
    { name: "About Us", href: "/about" },
    { name: "User", href: "/user" },
  ];

export default function Menu() {
    const pathname = usePathname();
  return (
    <ul className="flex flex-row text-[20px]">
        {menuHeader.map((item, index) => (
          <Link key={index} href={item.href}>
            <li 
            className={`${
              pathname === item.href ?  "bg-selected": ""
            } menu-header`}>
              {item.name}</li>
          </Link>
          ))}
          </ul>
  )
}
