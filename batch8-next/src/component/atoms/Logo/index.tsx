import Link from "next/link";
import React from "react";
import Image from "next/image";
import IMAGES from "@assets/images";

export default function Logo({ isDark = false }) {
  return (
    <div className="flex-1 px-5 py-2">
      <Link href="/" className="flex w-[200px]">
        {isDark ? (
          <Image
            src={IMAGES.logoBerijalanLight}
            alt="logo"
            width={200}
            height={50}
          />
        ) : (
          <Image
            src={IMAGES.logoBerijalan}
            alt="logo"
            width={200}
            height={50}
          />
        )}
      </Link>
    </div>
  );
}
