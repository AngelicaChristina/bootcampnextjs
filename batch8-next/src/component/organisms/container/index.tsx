"use client";

import Header from "@component/molecules/Header"
import Sidebar from "@component/molecules/Sidebar"

import { Provider } from 'react-redux'
import { store } from "@store/storage";

export default function Container({children} : {children: React.ReactNode}) {
  return (
    <Provider store={store}>
    <section>
      <Header />
      <div className="flex flex-row min-h-screen ">
      <Sidebar/>
    <div>{children}</div>
    </div>
    </section>
    </Provider>
  )
}
