"use client";

import Logo from "@component/atoms/Logo";
import Theme from "@component/atoms/Theme";
import Menu from "@component/atoms/Menu";
import SidebarIcon from "@component/atoms/Sidebaricon";
import { getItem, setItem } from "@store/storage";

import { usePathname } from "next/navigation";
import { useEffect, useState } from "react";

const withoutMenu = /^\/login|register$/i;

export default function Header() {
  const pathName = usePathname();
  

  const localTheme = getItem("_theme");
  const [isDark, setIsDark] = useState(localTheme === "dark");

  useEffect(() => {
    // setIsDark(localTheme === "dark");
    const getWindowsTheme = () => {
      return window.matchMedia("(prefers-color-scheme: dark)").matches;
    };
    setIsDark(getWindowsTheme);
  }, []);

  useEffect(() => {
    if (isDark) {
      document.body.classList.add("dark");
    } else {
      document.body.classList.remove("dark");
    }

    setItem("_theme", isDark ? "dark" : "light");
  }, [isDark]);

  if (withoutMenu.test(pathName)) return null; //kalo ada return hrus di atas return, hrus dibawah jangan diatas

  return (
    <header className="flex flex-row custom-shadow">
      {/* Sidebar */}
      <SidebarIcon />

      {/* logo */}
      <Logo isDark={isDark} />

      {/* theme */}
      <Theme isDark={isDark} onClick={() => setIsDark(!isDark)} />

      {/* menu */}
      <Menu />
    </header>
  );
}