// index.js pada page reducers

import dataCount from "./dataCount";
import { combineReducers } from "redux";

const rootReducer = combineReducers({
  dataCount,
});
export default rootReducer;