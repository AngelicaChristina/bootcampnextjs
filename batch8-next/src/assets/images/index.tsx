const IMAGES = {
    logoBerijalan: "/img/logo.png",
    logoBerijalanLight: "/img/logolight.png",
};

export default IMAGES;