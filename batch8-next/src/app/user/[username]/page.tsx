import satellite from "@services/satellite";
import Image from "next/image";

interface Params {
  params: { username: string };
}

interface Response {
  [key: string]: string | number | boolean | null;
  avatar_url: string;
}

// custom metadata
export function generateMetadata({ params }: Params) {
  return {
    title: params?.username,
  };
}

async function getPerson(username: string) {
  return satellite
    .get("https://api.github.com/users/" + username, {
      headers: {
        Authorization: "Bearer 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",
      },
    })
    .then((response) => {
      return response.data;
    })
    .catch((error) => {
      return error;
    });
}

export default async function UserDetails({ params: { username } }: Params) {
  const dataPerson: Response = await getPerson(username);
  return (
    <div className="flex flex-row min-h-screen px-5 pt-5">
      <div className="rounded-md w-[200px] h-[200px] p-8 mr-6 custom-shadow">
        <Image
          className="rounded-full custom-shadow"
          src={`${dataPerson.avatar_url}`}
          width={400}
          height={400}
          alt={"avatar_" + dataPerson.id}
        />
        <h1 className="mt-5 text-center text-[14px] font-semibold">
          @{username}
        </h1>
        <h1 className="flex flex-row  text-lg text-center gap-2 opacity-80 font-medium dark:opacity-25">
          {dataPerson.name}
        </h1>
      </div>
      <div className=" rounded-md w-full h-[20px] custom-shadow">
        <h1 className="flex flex-row gap-2 px-10 pt-5 mt-8 text-[18px]">
          Company :
          <section className=" text-[18px] opacity-70 dark:opacity-25">
            {dataPerson.company || "-"}
          </section>
        </h1>
        <h1 className="flex flex-row gap-2 px-10 pt-5 mt-2 text-[18px]">
          Location :
          <section className="text-[18px] opacity-70 dark:opacity-25">
            {dataPerson.location || "-"}
          </section>
        </h1>
        <h1 className="flex flex-row gap-2 px-10 pt-5 mt-2 text-[18px]">
          Biodata :
          <section className="text-[18px] opacity-70 dark:opacity-25">
            {dataPerson.bio || "-"}
          </section>
        </h1>
      </div>
    </div>
  );
}
