"use client";

import satellite from "@services/satellite";
import React from "react";
import { useState } from "react";
import Link from "next/link";
import useSWR from "swr";
import Image from "next/image";
import { StateRedux } from "@interface/interfaceRedux";
import { setDataCount } from "@store/actions/actionCount";

import {useSelector, useDispatch} from "react-redux";


const fetcher = (url: string) =>
  satellite
    .get(url, {
      headers: {
        Authorization: "Bearer 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",
      },
    })
    .then((res) => res.data);
export default function ListUser() {
  const dispatch = useDispatch();
  const dataCount = useSelector((state: StateRedux) => state.dataCount);

  const [seacrh, setSearch] = useState<string>('');
  const { data, error, isLoading } = useSWR(
    "https://api.github.com/search/users?q=" + seacrh,
    fetcher
  );
  const onSubmited = (event: React.FormEvent<HTMLFormElement>) => {
    event.preventDefault();

    const submitedValue = event.currentTarget[0] as HTMLInputElement;
    setSearch(submitedValue.value);
  };
  return (
    <div className='w-9/12'>
      <div>
        <h1>DATA COUNT REUDX: {dataCount.increment}</h1>
        <button onClick={()=> dispatch(setDataCount(dataCount.increment + 1))} className="bg-pink-400 py-2.5 px-8 rounded-lg ml-3 drop-shadow-m">
          Plus +
        </button>
        <button onClick={()=> dispatch(setDataCount(dataCount.increment - 1))} className="bg-violet-400 py-2.5 px-8 rounded-lg ml-3 drop-shadow-m">
          Minus -
        </button>
      </div>
      <form
        onSubmit={onSubmited}
        className="flex gap-5 justify-center w-[1000px] h-[45px] "
      >
        <input className="text-black text-[18px] w-full rounded-[8px] p-3 custom-shadow bg-[#f5f5f5]" />
        <button className="flex items-center justify-center rounded-[8px] bg-[#F6E58D] w-[100px] h-[45px] custom-shadow hover:bg-[#d9cb82]">
          <svg
            width="30"
            height="30"
            viewBox="0 0 40 40"
            fill="none"
            xmlns="http://www.w3.org/2000/svg"
          >
            <path
              d="M19.4806 3C28.5884 3 35.9613 10.3729 35.9613 19.4806C35.9613 28.5884 28.5884 35.9613 19.4806 35.9613C10.3729 35.9613 3 28.5884 3 19.4806C3 13.0619 6.66044 7.51049 12.021 4.78685"
              stroke="#132040"
              stroke-width="4.15185"
              stroke-linecap="round"
              stroke-linejoin="round"
            />
            <path
              d="M37.6961 37.6962L34.2265 34.2266"
              stroke="#132040"
              stroke-width="4.15185"
              stroke-linecap="round"
              stroke-linejoin="round"
            />
          </svg>
        </button>
      </form>
      {/* <div className="grid grid-cols-3 lg:grid-cols-9 md:grid-cols-6 sm "
      {error && <div> Error: {error.message}></div>}
      {isLoading && <div className="text-center"> Loading...</div>}
      {data?.items.map((item: any) ==> (
        
      ))} */}

      <div>
        {data?.items.map((item: any) => {
          return (
            <div
              key={item.id}
              className="flex flex-row my-4 p-4 rounded-md gap-4 custom-shadow"
            >
              <Image
                src={item.avatar_url}
                width={100}
                height={100}
                alt={item.login}
                className="rounded-md shadow-lg custom-shadow"
              />
              <Link
                href={"/user/" + item.login}
                className="text-xl font-semibold hover:text-slate-400"
              >
                {item.login}
              </Link>
            </div>
          );
        })}
      </div>
    </div>
  );
}
