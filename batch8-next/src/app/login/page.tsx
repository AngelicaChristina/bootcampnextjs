import React from 'react'
import FormLogin from './formLogin';

export const metadata = {
    title: "Login",
}

export default function Login() {
  return ( 
  <div className="flex flex-row w-screen min-h-screen items-center justify-center ">
    <FormLogin />
    </div>
  );
}