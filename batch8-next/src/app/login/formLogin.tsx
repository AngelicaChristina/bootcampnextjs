"use client";
import React from 'react'
import { yupResolver } from "@hookform/resolvers/yup";
import * as yup from "yup";
import { useForm } from "react-hook-form";
import submitForm from './submitForm';
import { useRouter } from 'next/navigation';


export interface FormLogin {
    email: string;
    password: string;
  }
  
  const schema = yup.object({
    email: yup
    .string()
    .email("Format email tidak sesuai")//mengubah validasi email di email()
    .required("Email harus di isi")
    .matches(/^(?![\w\.@]\.\.)(?![\w\.@]\.@)(?![\w\.]@\.)\w+[\w\.]@[\w\.]+\.\w{2,}$/i, //validasi email
    "Format email tidak sesuai"
    ), 
    password: yup.string().required("Password harus di isi"), //validasi yup
  });

export default function FormLogin() {
    const router = useRouter();
    const form = useForm<FormLogin>({
        defaultValues: {
          email: "",
          password: "",
        },
        resolver: yupResolver(schema),
        mode: "onChange", // kapan ketika validasi itu akan dipanggil 
      });
      const { 
        register, 
        handleSubmit, 
        formState: { errors },
    } = form;

    const onSubmited = (data : FormLogin) =>{
        console.log(data);
        submitForm(data). 
        then((res) => {
            console.log(res);
            router.push("/");
        })
        .catch(() => {
            alert("username atau password salah");
        });
    };

  return (
    <div className='flex flex-col custom-shadow p-5 w-[300px] gap-2 mx-auto border border-blue-900 bg-violet-400'>
    <form onSubmit={handleSubmit(onSubmited)} 
    className=' flex flex-col gap-2'>
        <h1 className='mt-2'>Email</h1>
        <input 
        type="email" 
        className='bg-pink-200 rounded-md p-2'
        placeholder='Email'
        {...register("email")}
        />
        <p className='text-red-700 text-sm'>{errors.email?.message}</p>

        <h1 className='mt-2'>Password</h1>
        <input 
        type="password" 
        className='bg-pink-200 rounded-md p-2'
        placeholder='Password'
        {...register("password")}
        />
        <p className='text-red-700 text-sm'>{errors.password?.message}</p>

        <button type="submit" className='bg-yellow-200 p-2 rounded-md'> 
        Login </button>
    </form>
    </div>
  )
}